<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //$token = Auth::user()->createToken('app')->accessToken;
       // $token = $this->createToken('app')->accessToken;
       $token="";
       if($this->can_request_token) {$token=$this->createToken('app')->accessToken;}
        return [
            'data'=>[
                'id'=>$this->id,
                'email'=>$this->email,
                'first_name'=>$this->first_name,
                'last_name'=>$this->last_name,
                'token'=>$token,
            ]
            ];
    }
}

<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ResetPassword;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests\CreateUserRequest;
use Illuminate\Auth\Events\PasswordReset;
use App\Http\Requests\ForgotPasswordRequest;

class AuthenticationController extends ApiController
{
    public function login(Request $request){

        if(Auth::guard()->attempt($request->only('email','password'))){
                $user=Auth::user();
               if($request['phpunittest']!='1') $user->can_request_token=true;
                return new UserResource($user);

        }
       else{
        return $this->errorResponse('Invalid username/password',401);
       }
    }
    public function register(CreateUserRequest $request){
       $data= $request->all();
       $data["password"]=Hash::make($data["password"]);
       $user=User::create($data);
        return new UserResource($user);
    }

    public function user(){

        return new UserResource(Auth::user());
    }

    public function forgot(ForgotPasswordRequest $request){
        Artisan::call('config:cache');
        $user= User::where('email','=',$request['email'])->get();
        if(count($user)==0)
        return $this->errorResponse('Email not Found',404);
        $token= Str::random(15);
        $message=DB::table('password_resets')->insert(
            [
            'email'=>$request['email'],
            'token'=>$token,
            ]);
              $email =$request['email'];
            Mail::send('mail.forgot',['token'=>$token],function($message) use ( $email){
                $message->to($email);
              //  $message->from('xyz@xyz.com');
                $message->subject('Reset your Password');
            });
             $message="Password request send to ".$request['email'] . ".";

            return $this->showMessage([
                "message"=>$message
            ],200);

    }

    public function reset(ResetPassword $request){
        $reset=DB::table('password_resets')
        ->where('token',$request['token'])
        ->get()->first();
        if(!$reset){
             return $this->errorResponse('Invalid Token',404);
        }
        $user=User::where('email','=',$reset->email)->first();
        $user->password=Hash::make($request['password']);
        $user->save();
        $reset=DB::table('password_resets')->where('token',$request['token'])
        ->delete();
        return new UserResource($user);

    }
    public function logout(Request $request){
        $request->user()->tokens()->delete();
    }

}

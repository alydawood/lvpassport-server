<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {


});
*/
Route::post('login','AuthenticationController@login');
Route::get('user','AuthenticationController@user')->middleware('auth:api');
Route::post('register','AuthenticationController@register');
Route::post('forgot','AuthenticationController@forgot');
Route::post('reset','AuthenticationController@reset');
Route::post('logout','AuthenticationController@logout')->middleware('auth:api');








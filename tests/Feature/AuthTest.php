<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    use RefreshDatabase;
     /** @test
      * @runTestsInSeparateProcesses
      */
    public function a_user_fails_to_login()
    {
        //$this->withoutExceptionHandling();
        $response = $this->post('/api/login',
        ['email'=>'test','password'=>'test']
      );
      $responseString = json_decode($response->getContent(), true);
     // dd($responseString);
      $response->assertStatus(401);
        $response->assertJson([
            "error" => "Invalid username/password",
            "code" => 401
          ]);

    }
     /** @test */
    public function a_user_can_login()
    {

        //$this->withoutExceptionHandling();
        factory(User::class,1)->create();
        $user=User::get()->first();
        $response = $this->post('/api/login',
        ['email'=>$user->email,'password'=>'password','phpunittest'=>'1']
      );
      //$responseString = json_decode($response->getContent(), true);

      $response->assertStatus(200);
      $response->assertJson(["data" => [
        "id" => $user->id,
        "email" => $user->email,
        "first_name" => $user->first_name,
        "last_name" => $user->last_name,
      ]
      ]);

    }
 /** @test */
    public function a_user_can_register()
    {

        $this->withoutExceptionHandling();

        $response = $this->post('/api/register',
        [
        'first_name'=>'FN',
        'last_name'=>'LN',
        'email'=>'email@email.com',
        'password'=>'password',
        'password_confirm'=>'password'


        ]
        );

        $response->assertJson(["data" => [
            'first_name'=>'FN',
        'last_name'=>'LN',
        'email'=>'email@email.com'
          ]
          ]);


      $response->assertStatus(201);
    }
    /** @test */
    public function a_user_cant_register_with_missing_fileds()
    {

       // $this->withoutExceptionHandling();

        $response = $this->post('/api/register',
        [
        'first_name'=>'',
        'last_name'=>'',
        'email'=>'',
        'password'=>'',
        'password_confirm'=>''


        ]
        );

        $responseString = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('first_name', $responseString['errors']['meta']);
        $this->assertArrayHasKey('last_name', $responseString['errors']['meta']);
        $this->assertArrayHasKey('email', $responseString['errors']['meta']);
        $this->assertArrayHasKey('password', $responseString['errors']['meta']);
        $this->assertArrayHasKey('password_confirm', $responseString['errors']['meta']);


      $response->assertStatus(422);

    }

    /** @test */
    public function password_must_match()
    {

       // $this->withoutExceptionHandling();

        $response = $this->post('/api/register',
        [
        'first_name'=>'AAA',
        'last_name'=>'AAAA',
        'email'=>'A@a.com',
        'password'=>'12345678',
        'password_confirm'=>'1234567'
        ]
        );
        $responseString = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('password_confirm', $responseString['errors']['meta']);
        $response->assertStatus(422);

    }

     /** @test */
     public function email_must_be_unique()
     {
        //$this->withoutExceptionHandling();
        factory(User::class,1)->create();
        $user=User::get()->first();
        // $this->withoutExceptionHandling();

         $response = $this->post('/api/register',
         [
         'first_name'=>'AAA',
         'last_name'=>'AAAA',
         'email'=>$user["email"] ,
         'password'=>'12345678',
         'password_confirm'=>'12345678'
         ]
         );
         $responseString = json_decode($response->getContent(), true);
         $this->assertArrayHasKey('email', $responseString['errors']['meta']);
         $response->assertStatus(422);

     }

     /** @test */
     public function can_request_password_reset()
     {
        $this->withoutExceptionHandling();
        factory(User::class,1)->create();
        $user=User::get()->first();
         $response = $this->post('/api/forgot',
         ['email'=> $user->email]
         );
        $responseString = json_decode($response->getContent(), true);


        $response->assertJson([
            "message" => "Password request send to ".$user->email. "."
          ]);
          $this->assertDatabaseHas('password_resets', [
            'email'=> $user->email,
        ]);
         $response->assertStatus(200);

     }

      /** @test */
      public function can_rest_password()
      {
         $this->withoutExceptionHandling();
         factory(User::class,1)->create();
         $user=User::get()->first();
          $response = $this->post('/api/forgot',
          ['email'=> $user->email]
          );
         $responseString = json_decode($response->getContent(), true);


         $response->assertJson([
             "message" => "Password request send to ".$user->email. "."
           ]);
           $this->assertDatabaseHas('password_resets', [
             'email'=> $user->email,
         ]);
         $reset=DB::table('password_resets')
        ->where('email',$user->email)
        ->get()->first();

         $response = $this->post('/api/reset',
         [
             'email'=> $user->email,
             'token'=> $reset->token,
             'password'=>'12345678',
             'password_confirm'=>'12345678'
         ]
         );
         $response->assertStatus(200);

      }

      /** @test */
      public function cant_rest_password_with_invalid_token()
      {
         $this->withoutExceptionHandling();
         factory(User::class,1)->create();
         $user=User::get()->first();
          $response = $this->post('/api/forgot',
          ['email'=> $user->email]
          );
         $responseString = json_decode($response->getContent(), true);


         $response->assertJson([
             "message" => "Password request send to ".$user->email. "."
           ]);
           $this->assertDatabaseHas('password_resets', [
             'email'=> $user->email,
         ]);

         $response = $this->post('/api/reset',
         [
             'email'=> $user->email,
             'token'=> 'invalid token',
             'password'=>'12345678',
             'password_confirm'=>'12345678'
         ]
         );
         $response->assertStatus(404);

      }

}
